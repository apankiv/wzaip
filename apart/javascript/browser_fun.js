var quickQuerySelector = function (selectString) {
    var parts = selectString.split(' ').filter(function (e) { return e !== ''});
    var result = process(parts[0], document);
    for (var i = 1; i < parts.length; i++) {
        result = process(parts[i], result);
    }
    return result;
}

var process = function (selector, result) {
    switch (selector[0]) {
        case '#':
            return result.getElementById(selector.substr(1));
            break;
        case '.':
            return result.getElementsByClassName(selector.substr(1))[0];
            break;
        default:
            return result.getElementsByTagName(selector.substr(1))[0];

    }
}

var start = new Date().getTime();

for (i = 0; i < 50000; ++i) {
    var a = jQuery('.sliderratevalue td')[0];
}

var end = new Date().getTime();
var time = end - start;
console.log('Execution time of jQuery: ' + time);

var start = new Date().getTime();

for (i = 0; i < 50000; ++i) {
    var a = document.querySelector('.sliderratevalue td');
}

var end = new Date().getTime();
var time = end - start;
console.log('Execution time of querySelector: ' + time);

var start = new Date().getTime();

for (i = 0; i < 50000; ++i) {
    var a = quickQuerySelector('.sliderratevalue td');
}

var end = new Date().getTime();
var time = end - start;
console.log('Execution time of quickQuerySelector: ' + time);

var start = new Date().getTime();

for (i = 0; i < 50000; ++i) {
    var a = document.getElementsByClassName('sliderratevalue')[0].getElementsByTagName('td');
}

var end = new Date().getTime();
var time = end - start;
console.log('Execution time of chaining: ' + time);


// Execution time of jQuery: 2311
// Execution time of querySelector: 438
// Execution time of quickQuerySelector: 86
// Execution time of chaining: 17
