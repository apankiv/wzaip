#include <iostream>
#include <cstdio>
#include <queue>

#define UP '^'
#define RIGHT '>'
#define DOWN 'v'
#define LEFT '<'
#define NO '-'
#define downDirection(c) (c == DOWN)
#define upDirection(c) (c == UP)
#define rightDirection(c) (c == RIGHT)
#define leftDirection(c) (c == LEFT)
#define directionCharacter(c) (rightDirection(c) || downDirection(c) || leftDirection(c) || upDirection(c))
#define freeCharacter(c) (c == '.')
#define blockedCharacter(c) (c == '#')
#define terminateCharacter(c) (c == 'C')
#define noDirection(c) (c == NO)
#define validCharacter(c) (directionCharacter(c) || freeCharacter(c) || blockedCharacter(c) || terminateCharacter(c))
#define inRange(x, y, r, c) (x >= 0 && x < r && y >= 0 && y < c)
#define directionChange(previousDirection, currentDirection) (noDirection(previousDirection) ? false : (previousDirection != currentDirection))

using namespace std;

char init[200][200], dirMap[200][200], finMap[200][200];
int distMap[200][200];

void find(int x, int y, int r, int c){
	queue<int> q;
	int nx = x, ny = y, a[] = {-1, 0, 1, 0}, b[] = {0, 1, 0, -1};
	char direction[] = {UP, RIGHT, DOWN, LEFT};
	if(upDirection(init[x][y]))
		nx--;
	if(rightDirection(init[x][y]))
		ny++;
	if(downDirection(init[x][y]))
		nx++;
	if(leftDirection(init[x][y]))
		ny--;
	distMap[nx][ny] = 0;
	dirMap[nx][ny] = init[x][y];
	q.push(nx);
	q.push(ny);
	while(!q.empty()){
		nx = q.front();
		q.pop();
		ny = q.front();
		q.pop();
		char previousDirection = dirMap[nx][ny];
		for(int i=0;i<4;i++){
			int tx = nx + a[i];
			int ty = ny + b[i];
			if(inRange(tx, ty, r, c)){
				char currentDirection = direction[i];
				int comparableDistance = distMap[nx][ny];
				if(directionChange(previousDirection, currentDirection))
					comparableDistance++;
				if(distMap[tx][ty] > comparableDistance){
					dirMap[tx][ty] = currentDirection;
					distMap[tx][ty] = comparableDistance;
					q.push(tx);
					q.push(ty);
				}
			}
		}
	}
}

void display(int r, int c){

	for(int i=0;i<r;i++){
		for(int j=0;j<c;j++)
			printf("%c", finMap[i][j]);
		printf("\n");
	}
}

char getMirror(char previousDirection, char currentDirection){
	switch(previousDirection){
		case UP:
			switch(currentDirection){
				case RIGHT:
					return '/';
				case LEFT:
					return '\\';
			}
		case RIGHT:
			switch(currentDirection){
				case UP:
					return '/';
				case DOWN:
					return '\\';
			}
		case DOWN:
			switch(currentDirection){
				case RIGHT:
					return '\\';
				case LEFT:
					return '/';
			}
		case LEFT:
			switch(currentDirection){
				case UP:
					return '\\';
				case DOWN:
					return '/';
			}
		default:
			return NO;
	}
}

int nextX(int x, char direction){
	if(direction == UP)
		return x + 1;
	if(direction == DOWN)
		return x - 1;
	return x;
}

int nextY(int y, char direction){
	if(direction == LEFT)
		return y + 1;
	if(direction == RIGHT)
		return y - 1;
	return y;
}

void findMinimumMirrorConfiguration(int x, int y, int r, int c, int sx, int sy){
	queue<int> q;
	q.push(x);
	q.push(y);
	while(!q.empty()){
		x = q.front();
		q.pop();
		y = q.front();
		q.pop();
		if(x == sx && y == sy)
			return;
		int nx = nextX(x, dirMap[x][y]);
		int ny = nextY(y, dirMap[x][y]);
		if(distMap[x][y] != distMap[nx][ny])
			finMap[nx][ny] = getMirror(dirMap[nx][ny], dirMap[x][y]);
		q.push(nx);
		q.push(ny);
	}
}

int main(){

	int t, r, c, startX, startY, endX, endY;
	char ch;
	scanf("%d", &t);
	while(t--){
		scanf("%d%d", &r, &c);
		for(int i=0;i<r;i++){
			for(int j=0;j<c;j++){
				scanf("%c", &init[i][j]);
				while(!validCharacter(init[i][j]))
					scanf("%c", &init[i][j]);
				finMap[i][j] = init[i][j];
				dirMap[i][j] = '-';
				if(directionCharacter(init[i][j])){
					startX = i;
					startY = j;
					distMap[i][j] = 0;
					dirMap[i][j] = init[i][j];
				} else if(terminateCharacter(init[i][j])){
					endX = i;
					endY = j;
					distMap[i][j] = 10000000;
				} else if(blockedCharacter(init[i][j])) {
					distMap[i][j] = -10000000;
				} else {
					distMap[i][j] = 10000000;
				}
			}
		}
		find(startX, startY, r, c);
		findMinimumMirrorConfiguration(endX, endY, r, c, startX, startY);
		display(r, c);
		printf("\n");
	}
	return 0;
}
