#include <iostream>
#include <stdint.h>
#include <vector>

using namespace std;

uint64_t gcd(uint64_t a, uint64_t b) {
    uint64_t r;
    while(b!=0) {
        r = a % b;
        a = b;
        b = r;
    }
    return a;
}

int main(){
    cin.tie(NULL);
    short int T;
    cin>>T;
    while(T-->0) {
        uint64_t smut=0;
        uint64_t N;
        cin>>N;
        vector<uint64_t> V;
        for(uint64_t i = 0; i < N; i++) {
            uint64_t K;
            cin>>K;
            V.push_back(K);
            for(int j=0; j<i; j++) {
                smut = smut+(V[i]|V[j]);
            }
        }
        uint64_t aleph=gcd(smut,N);
        smut /= aleph;
        N /= aleph;
        cout<<smut<<"/"<<N<<'\n';
    }
    return 0;
}
