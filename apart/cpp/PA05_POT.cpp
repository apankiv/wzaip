#include <cstdio>

using namespace std;

unsigned long long fast_pow(unsigned long long a,unsigned long long k, unsigned long long m){
    unsigned long long result = 1;
    unsigned long long power = k;
    unsigned long long value = a;
    unsigned long long mod = m;
    while(power>0)
    {
        if(power&1)
            {result = result*value;
            result = result%mod;}
        value = value*value;
        value = value%mod;
        power /= 2;
    }
    return result;
}

int  main () {
    unsigned long long t, a, k, m=10;
    scanf("%lld", &t);
    while (t --> 0) {
        scanf("%lld %lld", &a, &k);
        printf("%lld\n", fast_pow(a, k, m));
    }
}
