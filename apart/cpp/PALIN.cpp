#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

string np(string &a) {
    unsigned int l = a.length();
    unsigned int h = (l - 1) / 2;
    if (count(a.begin(), a.end(), '9') == a.length()) {
        string ret(l+1, '0');
        ret[0] = ret[l] = '1';
        return ret;
    }
    string ret(a);
    while (ret[h] == '9') { ret[l - 1 - h] = ret[h] = '0'; --h; }
    ret[l - 1 - h] = ret[h] = ret[h] + 1;
    return ret;
}

int main () {
    ios_base::sync_with_stdio(false);
    string y;
    unsigned int t, l;
    cin >> t;
    while (t --> 0) {
        cin >> y;
        string p(y);
        l = p.length();
        for (int i = 0; i <= (l - 1) / 2; ++i) p[l - 1 - i] = p[i];
        if (p > y) cout << p << '\n';
        else cout << np(p) << '\n';
    }
}
