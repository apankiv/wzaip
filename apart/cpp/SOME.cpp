#include <iostream>
#include <stdint.h>
#include <vector>
using namespace std;
int64_t modmul(int64_t a, int64_t b){
        int64_t result = (a*b)%1000000007;
        return result;
}
int64_t fast_pow(int64_t a, int k){
        int64_t result = 1;
        int64_t power = k;
        int64_t value = a;
        while(power>0)
        {
                if(power&1)
                {result = modmul(value,result); }
                value = modmul(value,value);
                power /= 2;
        }
        return result;
}

void newton(int A, int B,int C, int D, int64_t &coef1, int64_t &coef2){
        vector<int64_t> ans (1,1);
        int ither=0;
        if(A<C) {
                while(ither<A) {
                        ither++;
                        ans.push_back(1);
                        int64_t reserve=ans[0];
                        int64_t reserve2;
                        for(int j=1; j<ither/2+1; j++) {
                                reserve2=ans[j];
                                ans[j]=(reserve2+reserve)%1000000007;
                                reserve=reserve2;
                        }
                }
                coef1=ans[B];
                while(ither<C)
                {ither++;
                 ans.push_back(1);
                 int64_t reserve=ans[0];
                 int64_t reserve2;
                 for(int j=1; j<ither; j++) {
                         reserve2=ans[j];
                         ans[j]=(reserve2+reserve)%1000000007;
                         reserve=reserve2;
                 }}
                coef2=ans[D];
        }
        else{
                while(ither<C) {
                        ither++;
                        ans.push_back(1);
                        int64_t reserve=ans[0];
                        int64_t reserve2;
                        for(int j=1; j<ither; j++) {
                                reserve2=ans[j];
                                ans[j]=(reserve2+reserve)%1000000007;
                                reserve=reserve2;
                        }
                }
                coef2=ans[D];
                while(ither<A)
                {ither++;
                 ans.push_back(1);
                 int64_t reserve=ans[0];
                 int64_t reserve2;
                 for(int j=1; j<ither; j++) {
                         reserve2=ans[j];
                         ans[j]=(reserve2+reserve)%1000000007;
                         reserve=reserve2;
                 }}
                coef1=ans[B];
        }
}

int main(){
        ios_base::sync_with_stdio(false);
        cin.tie(NULL);
        uint64_t const M=1000000007;
        int N,A,B,D;
        while(cin>>N) {
                cin>>A>>B>>D;
                int64_t anspart1,anspart2;
                newton(N,A,B,D,anspart1,anspart2);
                anspart2=fast_pow(anspart2,A);
                cout<<modmul(anspart1,anspart2)<<'\n';
        }
        return 0;
}
