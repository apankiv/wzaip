#include <iostream>
#include <cstdio>

using namespace std;

bool w[1000000];
int sqr[1000];

int main(){
	fill(w, w + 1000000, false);
	w[0] = false;
	for(int i = 0; i < 1000; ++i) sqr[i] = (i+1) * (i+1);
	for(int i = 0; i < 1000000; ++i){
		for(int j = 0; sqr[j] <= i; ++j){
			w[i] |= (!w[i - sqr[j]]);
			if(w[i]) break;
		}
	}
	int t, n;
	scanf("%d", &t);
	while(t--){
		scanf("%d", &n);
		printf("%s\n", w[n] ? "Win" : "Lose");
	}
	return 0;
}
