#include <iostream>

using namespace std;

int main () {
    int a[10], i = 0, tt;
    char t;
    while (cin >> t) {
        if (t == '-') {
            if (i == 0) cout << ":(\n";
            else {
                cout << a[--i] << '\n';
            }
        }
        if (t == '+') {
            cin >> tt;
            if (i < 10) {
                a[i++] = tt;
                cout << ":)\n";
            } else {
                cout << ":(\n";
            }
        }
    }
    return 0;
}
