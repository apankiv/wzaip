#include <cstdio>

int main () {
    int T, N;
    double sum;
    scanf("%d", &T);
    while (T --> 0) {
        scanf("%d", &N);
        sum = 0;
        for (int i = 1; i <= N; i++) sum += 1.0/i;
        printf("%.2f\n", N*sum);
    }
    return 0;
}
