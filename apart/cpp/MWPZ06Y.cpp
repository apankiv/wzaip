#include <cstdio>
#include <vector>
#include <string>

using namespace std;

vector<int> fib;

int f (int n) {
    while (fib.size() < n) {
        fib.push_back(fib[fib.size() - 2] + fib[fib.size() - 1]);
    }

    return fib[n-1];
}

string solve (string a, int n, int k) {
    if (f(n) < k) {
        return "-1";
    }

    if (n == 0) {
        return a;
    }

    return (f(n-1) < k) ? solve(a + "1", n-1, k - f(n-1)) : solve(a + "0", n-1, k);
}

int main () {
    int t, n, k;
    fib.push_back(2);
    fib.push_back(3);
    scanf("%d", &t);

    while (t --> 0) {
        scanf("%d %d", &n, &k);
        printf("%s\n", solve("", n, k).c_str());
    }

    return 0;
}
