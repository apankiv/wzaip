#include <bits/stdc++.h>
#include <stdint.h>

using namespace std;


void test(vector<int> const &M) {
  int n;
  cin >> n;
  cout << M[n] << '\n';
}


int main() {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);

  vector<bool> P(1000001, true);

  P[0] = P[1] = false;

  for(int i = 2; i <= 1000; ++i) {
    if(not P[i]) continue;
    for(int j = i*i; j <= 1000000; j+=i) P[j] = false;
  }

  int p = 1, m = 10;
  for(int i = 10; i <= 1000000; ++i) {
    if(i >= m * 10) { p = m; m *= 10; }
    int t = i % m;
    if(not P[t] or t < p) P[i] = false;
  }

  vector<int> M(1000001);
  for(int i = 1; i <= 1000000; ++i) M[i] = M[i-1] + P[i];

  int t;
  cin >> t;
  while(t --> 0) test(M);

  return 0;
}
