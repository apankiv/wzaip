#include <cstdio>
#include <cstring>

using namespace std;

#define ull unsigned int

void mul (ull a[][2], ull b[][2]) {
    ull res[2][2];
    memset(res , 0, sizeof res);
    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 2; j++)
            for (int k = 0; k < 2; k++)
                res[i][j] = (res[i][j] + a[i][k] * b[k][j]) % 10;

    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 2; j++)
            a[i][j] = res[i][j];
}

ull pow(ull n) {
    ull fib[2][2] = { {1 , 1} ,
                      {1 , 0} },
        tmp[2][2] = { {1 , 0} ,
                      {0 , 1} };

    while (n) {
        if (n % 2) mul(tmp, fib);
        mul(fib, fib);

        n >>= 1;
    }

    return tmp[0][1];
}

void test () {
    ull n, a, b;
    scanf("%d %d %d", &n, &a, &b);
    printf("%d\n", (pow(n-2) * a + b * pow(n-1)) % 10);
}

int main () {
    int T;
    scanf("%d", &T);

    while (T --> 0) {
        test();
    }

    return 0;
}
