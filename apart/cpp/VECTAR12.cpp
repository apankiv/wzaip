#include <iostream>
#include <cstdio>

using namespace std;

int arr[7000][7000];

inline int inRange(int x, int y, int r, int c) { return x >= 0 && x < r && y >= 0 && y < c && y <= x; }

void preCalculate(){
	arr[0][0] = 1;
	arr[1][0] = 1;
	arr[1][1] = 1;
	for(int i=2;i<7000;i++){
		for(int j=0;j<=i;j++){
			arr[i][j] = 0;
			if(inRange(i-1, j-1, 7000, 7000))
				arr[i][j] = (arr[i][j] + arr[i-1][j-1]) % 1000000007;
			if(inRange(i-1, j, 7000, 7000))
				arr[i][j] = (arr[i][j] + arr[i-1][j]) % 1000000007;
			if(inRange(i-1, j+1, 7000, 7000))
				arr[i][j] = (arr[i][j] + arr[i-1][j+1]) % 1000000007;
		}
	}
}

int main(){
	preCalculate();
	int t, n, k;
	scanf("%d", &t);
	while(t--){
		scanf("%d %d", &n, &k);
		printf("%d\n", arr[n][k]);
	}
	return 0;
}
