#include <sstream>
#include <string>
#include <cstdio>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

void split(vector<string>* v, string line, char splitSymbol = ';') {
    string tmp = "";
    for (int i = 0; i < line.size(); i++) {
        if (line[i] == splitSymbol) {
            v->push_back(tmp);
            tmp = "";
            continue;
        } else {
            tmp += line[i];
        }
    }
}

bool exists(string a, vector<string> *arr) { return find(arr->begin(), arr->end(), a) != arr->end(); }

int max(int a, int b) { return a > b ? a : b; }

int main(int argc, char* argv[]) {
    if (argc < 3) {
        printf("Usage: %s filename userid\n", argv[0]);
        return 1;
    }

    ifstream infile(argv[1]);
    string line;
    vector<vector<int>> matrix;
    vector<string> splitted;
    map<string, int> userid;
    map<string, int> showid;
    map<int, string> idshow;
    int uid, sid;

    while (getline(infile, line)) {
        split(&splitted, line);
        auto element = userid.find(splitted[0]);

        if (element == userid.end()) {
            int size = 0;

            if(matrix.size() > 0){
                size = matrix[0].size();
            }

            vector<int> newUser(size);
            matrix.push_back(newUser);
            userid.insert(pair<string, int>(splitted[0], matrix.size() - 1));
            uid = matrix.size() - 1;
        } else {
            uid = element->second;
        }

        // In a matter of fact it's a bit bad for just shows so let's join them
        element = showid.find(splitted[1]+"_"+splitted[2]);

        if (element == showid.end()) {
            for (auto i = matrix.begin(); i != matrix.end(); i++) {
                (*i).push_back(0);
            }
            showid.insert(pair<string, int>(splitted[1]+"_"+splitted[2], matrix[0].size() - 1));
            idshow.insert(pair<int, string>(matrix[0].size() - 1, splitted[1]+"_"+splitted[2]));
            sid = matrix[0].size() - 1;
        } else {
            sid = element->second;
        }

        matrix[uid][sid]++;

        splitted.clear();
    }

    vector<vector<int>> prod1(matrix[0].size());

    for (auto i = prod1.begin(); i != prod1.end(); i++) i->resize(matrix[0].size());

    for(int i = 0; i < matrix[0].size(); i++) {
        for (int j = 0; j < matrix[0].size(); j++) {
            for (int k = 0; k < matrix.size(); k++) {
                prod1[i][j] += matrix[k][i] * matrix[k][j];
            }
        }
    }

    vector<vector<int>> prod2(matrix[0].size());

    for (auto i = prod2.begin(); i != prod2.end(); i++) i->resize(matrix.size());

    for(int i = 0; i < prod1.size(); i++) {
        for (int j = 0; j < matrix.size(); j++) {
            for (int k = 0; k < matrix.size(); k++) {
                prod2[i][j] += prod1[i][k] * matrix[k][j];
            }
        }
    }

    for (int i = 0; i < matrix.size(); i++) {
        for (int j = 0; j < matrix[0].size(); j++) {
            if (matrix[i][j] > 0) prod2[j][i] = 0;
        }
    }

    uid = userid.find(argv[2])->second;
    vector<string> shows;

    for (int i = 0; i < prod2.size(); i++) {
        if (prod2[i][uid] > 0) shows.push_back(idshow.find(i)->second);
    }

    for (auto i = shows.begin(); i != shows.end(); i++) printf("%s\n", (*i).c_str());

    return 0;
}
