-- Zadanie 1a
factReg :: Integer -> Integer
factReg 0 = 1
factReg x = x * factReg (x - 1)

main1a = do
    line <- getLine
    let n = read line :: Integer
    putStrLn $ show $ factReg n

-- Zadanie 1b
factMod :: Integer -> Integer
factMod 0 = 1
factMod x = (x * factMod (x - 1)) `mod` 1000

main1b = do
    line <- getLine
    let n = read line :: Integer
    if n > 14 then do
        putStrLn "000"
        else do
            putStrLn $ show $ factMod n

-- Zadanie 2a
f1 :: Double -> Double
f1 x = (-1)**(x + 1) / (2 * x - 1)

main2a = do
    line <- getLine
    let n = read line :: Double
    putStrLn $ show $ 4 * (sum $ map f1 [1.0..n])

-- Zadanie 2b
f2 :: Double -> Double
f2 1.0 = sqrt 2
f2 x   = 2 / (sqrt $ 2 * (1 + 1 / (f2 $ x - 1)))

main2b = do
    line <- getLine
    let n = read line :: Double
    putStrLn $ show $ 2 * (product $ map f2 [1.0..n])

-- Zadanie 3
main3 = do
    let sp = [' ',' '..]
    let hs = ['#', '#'..]
    line <- getLine
    let (w:h:s:_) = map (read::String->Int) $ words line
    let arr = cycle $ (take s sp) ++ (take s hs)
    printFg arr w h s

printFg a w 0 s = do
    return ()

printFg a w h s = do
    printLn (take (s*w) a) s
    printFg (drop s a) w (h - 1) s

printLn a 0 = do
    return ()

printLn a x = do
    putStrLn a
    printLn a $ x - 1
