main = do
    a <- getLine
    test $ read a

test 0 = do
    return ()
test x = do
    s <- getLine
    let (n:a:b:_) = map (read::String->Int) $ words s
    print $ fib (trueMod $ n - 3) (a `mod` 10) (b `mod` 10)
    test $ x - 1

fib 0 a b = (a + b) `mod` 10
fib x a b = fib (x-1) b ((a+b) `mod` 10)

trueMod x
    | x `mod` 60 == 0 = 60
    | otherwise = x `mod` 60
