import Data.Maybe
import Data.Array
import qualified Data.ByteString.Lazy.Char8 as BS

primes' a b
    | b < a = []
    | b < 2 = []
    | otherwise = (if a < 3 then [2] else []) ++ [i | i <- [o,o+2..b], ar ! i]
    where
        o = max (if even a then a + 1 else a) 3
        r  = floor . sqrt $ fromIntegral b + 1
        ps = primes' 2 r
        ar = accumArray (\a b -> False) True (o,b) ls
        ls = [(i,()) | p <- ps
                       , let q = p*p
                             s = 2*p
                             (n,x) = quotRem (o-q) s
                             q' = if o <= q then q
                                            else q+(n+signum x)*s
                       , i <- [q',q'+s..b]]

readInt = fst . fromJust . BS.readInt

solve :: BS.ByteString -> BS.ByteString
solve input = let [m,n] = map readInt $ BS.words input
                  in BS.unlines $ map (BS.pack . show) $ primes' m n

main = do
    input <- getLine
    let testcount = read input
    sequence . take testcount . repeat $ do
        input <- getLine
        BS.putStrLn $ solve $ BS.pack input
