mylast :: [a] -> a
mylast (_:xs) = mylast xs
mylast ([x])  = x
mylast []     = error "No end for empty lists!"
