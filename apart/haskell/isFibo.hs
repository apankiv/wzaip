-- Enter your code here. Read input from STDIN. Print output to STDOUT
main :: IO()
main = do
    line <- getLine
    test $ (read line :: Integer)

isPerfectSquare :: Integer -> Bool
isPerfectSquare x = sq * sq == x
    where sq = floor $ sqrt $ (fromIntegral x :: Double)

isFib :: Integer -> Bool
isFib x = (isPerfectSquare (5*x*x + 4) || isPerfectSquare (5*x*x - 4))

test 0 = do
    return()

test x = do
    line <- getLine
    if isFib $ (read line :: Integer)
        then putStrLn ("IsFibo")
        else putStrLn ("IsNotFibo")
    test (x-1)
