--import qualified Data.Char (chr)

main :: IO()
main = do
    printPI mypi

printPI (x:xs) = do
    putChar $ Data.Char.chr (48+x)
    putChar '.'
    printRest xs 12000

printRest (x:xs) 0 = do
    return ()

printRest (x:xs) n = do
    putChar $ Data.Char.chr (48+x)
    printRest xs (n-1)

mypi = g(1,0,1,1,3,3) where
    g(q,r,t,k,n,l) =
        if 4*q+r-t<n*t
        then n : g(10*q,10*(r-n*t),t,k,div(10*(3*q+r))t-10*n,l)
        else g(q*k,(2*q+r)*l,t*l,k+1,div(q*(7*k+2)+r*l)(t*l),l+2)
