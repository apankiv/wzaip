main :: IO()
main = do
    t <- getLine
    test (read t)

test :: Int -> IO()
test 0 = do
    return()
test x = do
    line <- getLine
    if line == (reverse line)
        then putStrLn $ nextpalindrome (show $ (read line) + 1) "" ""
        else putStrLn $ nextpalindrome line "" ""
    test (x - 1)

nextpalindrome :: String -> String -> String -> String
nextpalindrome x as bs | ((read x) `div` 10 == 0) = as ++ x ++ bs
                       | ((length $ init $ tail x) == 0) = as ++ bs
                       | ((init x) == (last x)) = nextpalindrome (read $ init $ tail x) (as ++ [(head x)]) ([(last x)] ++ bs)
