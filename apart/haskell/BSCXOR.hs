main :: IO ()
main = do
    b <- getLine
    let arr = map stringToBool (words b)
    print . toStr $ (arr !! 0) `xor` (arr !! 1)

stringToBool "1" = True
stringToBool "0" = False

toStr a
    | a == True = 1
    | otherwise = 0

xor a b = not (a == b)
