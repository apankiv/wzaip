main :: IO ()
main = do
    line <- getLine
    test $ (read line :: Integer)

test :: Integer -> IO ()
test 0 = do
    return ()
test x = do
    line <- getLine
    putStrLn $ removeLeadingZeros . reverse . show $ sum $ map ((read::String->Int) . reverse) $ words line
    test (x - 1)

removeLeadingZeros :: [Char] -> [Char]
removeLeadingZeros (x:xs)
    | x == '0' = removeLeadingZeros xs
    | otherwise = x:xs
