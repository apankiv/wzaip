main :: IO ()
main = do
    a <- getLine
    let arr = [foldr modprod 1 [1..x] | x <- [1..1000]]
    test arr $ read a

modprod x y =
    if (x * y) `mod` 10 == 0
        then modprod 1 $ (x * y) `div` 10
        else (x * y) `mod` 10000

test xs 0 = do
    return()
test xs x = do
    b <- getLine
    print $ (xs !! ((read b) - 1)) `mod` 10
    test xs $ x - 1
