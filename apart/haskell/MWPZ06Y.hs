main = do
    a <- getLine
    test $ read a

shitFibonacci 1 = 2
shitFibonacci 2 = 3
shitFibonacci x = shitFibonacci (x - 1) + shitFibonacci (x - 2)

test 0 = do
    return ()
test x = do
    s <- getLine
    let (n:k:_) = map (read::String->Int) $ words s
    print $ solve "" n k [shitFibonacci(x) | x <- [1..44]]
