main = do
    a <- getLine
    let (s:e:_) = map (read::String->Int) $ words a
    print $ sum [x*x | x <- [s..e]]
