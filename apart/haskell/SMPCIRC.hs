main :: IO()
main = do
    a <- getLine
    test $ read a

split :: [a] -> ([a], [a])
split myList = splitAt (((length myList) + 1) `div` 2) myList

test :: Int -> IO()
test 0 = do
    return()
test x = do
    l <- getLine
    let arr = map (read::String->Int) $ words l
    let coord = split arr
    print coord
    test (x-1)
