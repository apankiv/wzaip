m = int(input('m = '))
a = list(input('input nodes: '))

def find_closest_modulo(n):
    while not n in a:
        n += 1
        n = n % 2**m

    return n

def generate_fingertable(n):
    for p in range(m):
        print '{0:2d} {1:2d}'.format(p, find_closest_modulo(n+2**p))

for i in a:
    print 'Generated for {0:2d}'.format(i)
    generate_fingertable(i)
