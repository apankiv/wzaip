#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

void solve (string a, string b) {
    // int lastPosA = lastPosB = 0;
    int lastDiff = 0;
    for (int i = 0; i < a.length(); i++) {
        if (a[i] == '?') {
            if (b[i] == '?') {
                if (lastDiff == 0) {
                    a[i] = 48;
                    b[i] = 48;
                } else if (lastDiff > 0){
                    a[i] = 48 + 9;
                    b[i] = 48;
                } else {
                    a[i] = 48;
                    b[i] = 48 + 9;
                }
            } else {
                a[i] = b[i];
            }
        } else {
            if (b[i] == '?') {
                b[i] = a[i];
            } else {
                lastDiff = b[i] - a[i];
            }
        }
    }
    cout << a << " " << b << "\n";
}

int main () {
    int T;
    string a, b;
    cin >> T;

    for (int i = 1; i <= T; ++i) {
        cin >> a >> b;
        cout << "Case #" << i << ": ";
        solve(a, b);
    }

    return 0;
}
