#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

string numbers[] = {"ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE"};

int find(int index, string &number) {
    bool flag = true, found = false;
    string copy = number;
    for (int i = 0; i < numbers[index].length() && flag; i++) {
        for (int j = 0; j < copy.length(); j++) {
            flag = false;
            if (numbers[index][i] == copy[j]) {
                flag = true;
                copy[j] = '-';
                break;
            }
        }
    }
    if (flag) {
        copy.erase (std::remove(copy.begin(), copy.end(), '-'), copy.end());
        number = copy;
        return index;
    }
    return -1;
}

void solve (string a, int pos) {
    string ans = "";
    string copy = a;
    int t = 0, p = pos;
    bool found = true;
    do {
        for (int i = pos; i < 10; i++) {
            t = find(i, a);
            // cout << found << " " << t << " " << a << endl;
            if (t > -1) {
                pos = t;
                found = true;
                ans = ans + (char)(48+t);
                break;
            } else {
                found = false;
            }
        }
    } while (found && a.length() > 0);

    if (a.length() > 0) {
        solve(copy, p+1);
    } else {
        cout << ans << "\n";
    }
}

int main () {
    int T;
    string a;
    cin >> T;

    for (int i = 1; i <= T; ++i) {
        cin >> a;
        cout << "Case #" << i << ": ";
        solve(a, 0);
    }

    return 0;
}
