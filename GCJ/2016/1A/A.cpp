#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

void solve (string a) {
    string res = "";
    for (int i = 0; i < a.length(); i++) {
        res = max(res + a[i], a[i] + res);
    }
    cout << res << "\n";
}

int main () {
    int T;
    string a;
    cin >> T;

    for (int i = 1; i <= T; ++i) {
        cin >> a;
        cout << "Case #" << i << ": ";
        solve(a);
    }

    return 0;
}
