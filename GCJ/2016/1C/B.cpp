#include <cstdio>


int main() {
  int t;
  int b;
  long long m;
  long long l;
  scanf("%d", &t);

  for(int c = 0; c < t; c++) {
    printf("Case #%d: ", c + 1);
    scanf("%d %lld", &b, &m);
    l = 1LL << (b - 2);

    if(m > l) {
      puts("IMPOSSIBLE");
      continue;
    } else {
      puts("POSSIBLE");
    }

    for(int row = 0; row < b; row++) {
      for(int col = 0; col < b; col++) {
        if(col != b - 1 or m == l) {
          putchar(col > row ? '1' : '0');
        } else {
          if(row == 0) {
            putchar('0');
          } else {
            putchar('0' + ((m >> (row - 1)) & 1));
          }
        }
      }
      putchar('\n');
    }
  }

  return 0;
}
