#include <iostream>
#include <set>
#include <map>

using namespace std;


template<typename t_c, typename t_e>
bool has(t_c const &container, t_e const &elem) {
	return container.find(elem) != container.end();
}


struct opt {
	int x, y, z;
	opt(int a, int b, int c): x(a), y(b), z(c) { }

	bool operator<(opt const &a) const {
		if(x < a.x) return true;
		if(y < a.y) return true;
		if(z < a.z) return true;
		return false;
	}

	friend ostream &operator<<(ostream &os, opt const &r) {
		return (os << r.x << ' ' << r.y << ' ' << r.z);
	}
};


void test() {
	static int c;
	cout << "Case #" << ++c << ": ";

	int j, p, s, k;
	cin >> j >> p >> s >> k;

	set<opt> res;
	map<opt, int> tgc;

	int jo = 0, po = 0, so = 0;

	for(bool loop = true; loop;) {
		loop = false;
		for(int jjj = 1; jjj <= j; ++jjj) {
    		int jj = (jjj + jo - 1) % j + 1;
    		for(int ppp = 1; ppp <= p; ++ppp) {
        		int pp = (ppp + po - 1) % p + 1;
        		if(tgc[opt(jj, pp, 0)] >= k) continue;
        		for(int sss = 1; sss <= s; ++sss) {
        		    int ss = (sss + so - 1) % s + 1;
        			if(has(res, opt(jj, pp, ss))) continue;
        			if(tgc[opt(jj, pp, 0)] >= k) continue;
        			if(tgc[opt(jj, 0, ss)] >= k) continue;
        			if(tgc[opt(0, pp, ss)] >= k) continue;

        			jo += 1;
        			po += 1;
        			so += 1;

        			tgc[opt(jj, pp, 0)]++;
        			tgc[opt(jj, 0, ss)]++;
        			tgc[opt(0, pp, ss)]++;
        			res.insert(opt(jj, pp, ss));

        			loop = true;
        		}
    		}
		}
	}

	cout << res.size() << '\n';

	for(auto it = res.begin(); it != res.end(); ++it) {
		cout << *it << '\n';
	}
}


int main() {
	int t;
	cin >> t;

	while(t --> 0) test();

	return 0;
}
