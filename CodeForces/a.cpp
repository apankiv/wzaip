#include <iostream>
#include <vector>

using namespace std;

int main () {
    vector<int> a;
    int t, tt, max = 0, sum = 0;
    cin >> t;
    while(t-->0) {
        cin >> tt;
        if (tt > max) max = tt;
        a.push_back(tt);
    }
    for(auto it = a.begin(); it != a.end(); ++it) {
        if (*it < max) sum += max - *it;
    }
    cout << sum << "\n";
    return 0;
}
