#include <iostream>

using namespace std;

#define endl "\n"

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    bool arr[100001];
    int n, t;
    cin >> n;
    for (int i = 0; i < 100001; ++i) arr[i] = false;
    for (int i = n; i > 0; --i) {
        cin >> t;
        arr[t] = true;
        if (t == n) {
            int j;
            for (j = t; arr[j] == true; --j ) cout << j << " ";
            cout << endl;
            n = j;
        } else {
            cout << endl;
        }
    }
    return 0;
}
