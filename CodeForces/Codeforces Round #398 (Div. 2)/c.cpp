#include <iostream>
#include <vector>

using namespace std;


struct Vertex: vector<int> {
    int l;
    int sum;
    int p;
    int d;
    Vertex(): sum(0), d(-1) { }
};

typedef vector<Vertex> graph;


bool dfs(graph &g, vector<int> &s, int r, int const &t) {
    int n = g.size();
    vector< pair<int, int> > D(n);
    int d = 0;
    D[d++] = make_pair(r, 0);

    while(d > 0) {
        int u = D[d - 1].first;
        int i = D[d - 1].second;

        if(i < g[u].size()) {
            D[d - 1].second++;

            int v = g[u][i];
            D[d++] = make_pair(v, 0);
        } else {
            d--;

            g[u].sum = g[u].l;

            for(int j = 0; j < g[u].size(); j++) {
                int v = g[u][j];
                g[u].sum += g[v].sum;
                if(g[v].d != -1) g[u].d = g[v].d;
            }

            if(g[u].d != -1 and g[u].p != -1 and g[u].sum == t * 2) {
                cout << g[u].d + 1 << ' ' << u + 1 << endl;
                return true;
            }

            if(g[u].d == -1 and g[u].sum == t) {
                g[u].d = u;
                s.push_back(u);
            }
        }
    }

    return false;
}


int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n;
    cin >> n;

    graph g(n);
    int r = -1;
    int sum = 0;

    for(int i = 0; i < n; i++) {
        int p;
        cin >> p >> g[i].l;
        sum += g[i].l;
        p--;
        g[i].p = p;
        if(p == -1) r = i;
        else g[p].push_back(i);
    }

    if(sum % 3 != 0) {
        cout << "-1" << endl;
        return 0;
    }

    vector<int> s;
    if(dfs(g, s, r, sum / 3)) return 0;

    if(s.size() < 2) {
        cout << "-1" << endl;
    } else {
        cout << s[0] + 1 << ' ' << s[1] + 1 << endl;
    }

    return 0;
}
