#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

inline int max (int a, int b) {
    return a > b ? a : b;
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n, t;
    vector<int> a;
    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> t;
        a.push_back(t);
    }
    if (n < 3) {
        cout << "NO\n";
        return 0;
    }
    sort(a.begin(), a.end());
    for (int i = 2; i < a.size(); ++i) {
        if (a[i-2] + a[i-1] > a[i]) {
            cout << "YES\n";
            return 0;
        }
    }
    cout << "NO\n";
    return 0;
}
