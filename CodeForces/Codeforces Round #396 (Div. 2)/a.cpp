#include <iostream>
#include <string>

using namespace std;

inline int max (int a, int b) {
    return a > b ? a : b;
}

int main () {
    string a, b;
    cin >> a >> b;
    if (a == b) {
        cout << "-1\n";
    } else {
        cout << max (a.length(), b.length()) << "\n";
    }
    return 0;
}
