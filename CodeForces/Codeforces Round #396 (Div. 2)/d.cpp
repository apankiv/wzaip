#include <iostream>
#include <vector>
#include <map>

using namespace std;

int find(vector<int> &R, int a) {
  return (R[a] == a) ? a : (R[a] = find(R, R[a]));
}


bool same(vector<int> &R, int a, int b) {
  return find(R, a) == find(R, b);
}


bool opposite(vector<int> &R, int a, int b) {
  int n = R.size() / 2;
  return same(R, a, n + b);
}


void join(vector<int> &R, int a, int b) {
  R[find(R, a)] = find(R, b);
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int a, b, c, t;
    string t1, t2;
    cin >> a >> b >> c;
    map<string, int> w;
    for (int i = 0; i < a; ++i) {
        cin >> t1;
        w[t1] = i;
    }
    vector<int> r(2*a);
    for (int i = 0; i < r.size(); ++i) r[i] = i;

    for (int i = 0; i < b; ++i) {
        cin >> t >> t1 >> t2;
        if (t == 1) {
            if (not opposite(r, w[t1], w[t2])) {
                cout << "YES\n";
                join(r, w[t1], w[t2]);
                join(r, w[t1] + a, w[t2] + a);
            } else {
                cout << "NO\n";
            }
        } else {
            if (not same(r, w[t1], w[t2])) {
                cout << "YES\n";
                join(r, w[t1], w[t2] + a);
                join(r, w[t1] + a, w[t2]);
            } else {
                cout << "NO\n";
            }
        }
    }

    for (int i = 0; i < c; ++i) {
        cin >> t1 >> t2;
        if (same(r, w[t1], w[t2])) {
            cout << "1\n";
        } else {
            if (opposite(r, w[t1], w[t2])) {
                cout << "2\n";
            } else {
                cout << "3\n";
            }
        }
    }

    return 0;
}
