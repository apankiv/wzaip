#include <iostream>
#include <cmath>
#include <limits>
#include <vector>
#include <queue>
#include <limits.h>
#include <stdint.h>
using namespace std;

class node;

class chan {
    public:
        int w;
        bool flag;
        chan *e;
        node *t;
        chan() : w(0) {}
        chan(node *t) : t(t), w(0), flag(true){}
        chan(node *t, chan *e) : t(t), e(e), flag(false) {}
        virtual int weight() const { return (flag) ? w : 0; }
        int diff() const { return cap()-weight(); }
        virtual int cap() const { return (flag) ? INT_MAX : e->weight(); }
        virtual void change_weigth(int add) { if (flag) w += add; else e->change_weigth(-1*add); }
};

class node : public vector<chan*> {
    public:
        node::iterator other;
        int pathDist;
};

class os : public chan, public node {
    public:
        int IQ;
        bool replay;
        os() : chan(), replay(false) {}
        int weight() const { return chan::w; }
        void change_weigth(int add) { chan::w+= add; }
        int cap() const { return IQ; }
};


bool bfs(node *sr,node *pr, os *persons, int n, int m) {
    queue<node*> line;
    line.push(sr);

    sr->pathDist = 0;
    pr->pathDist = -1;
    for (os *p = persons; p != persons+n+m; p++) p->pathDist = -1;

    while (!line.empty()) {
        node *v = line.front();
        line.pop();
        for (node::iterator it = v->begin(); it != v->end(); ++it) {
            if( ((*it)->diff() > 0) && ( (*it)->t->pathDist == -1) ) {
                (*it)->t->pathDist = v->pathDist+1;
                line.push((*it)->t);
            }
        }
    }
    return pr->pathDist != -1;
}

long long dfs(node *sc,node *pr,long long max) {
    if( sc == pr ) {
        return max;
    } else {
        int ret = 0;
        while((sc->other != sc->end()) && (max != 0)) {
            chan* e = *(sc->other);
            if(!( (e->t->pathDist) != sc->pathDist+1 || (e->diff() == 0) )) {
                long long sub_flow = dfs( e->t, pr, min(max, (long long) e->diff()));
                e->change_weigth(sub_flow);
                ret += sub_flow;
                max -= sub_flow;
            }
            sc->other++;
        }
        return ret;
    }
}


int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int inf, mat;
    node pr,sr;

    cin >> inf >> mat;

    os *persons = new os[inf+mat], *uc = persons-1, *vert = uc+inf;
    vector < vector<bool> > es (inf+1,vector<bool> (mat+1));

    for( int i = 1; i <= inf; i++ ) {
         uc[i].t = uc+i;
         sr.push_back(uc+i);
    }

    for (int i = 1; i <= mat; i++) {
         vert[i].t = &pr;
         vert[i].push_back(vert+i);
    }

    int in,in1,pair_size;
    cin >> pair_size;
    while( pair_size --> 0 ) {
        cin >> in >> in1;
        es[in][in1] = true;
    }


    for (int a = 1; a <= inf; a++) {
        for (int b = 1; b <= mat; b++) {
            if(!es[a][b]) {
                chan *e = new chan(vert+b);
                uc[a].push_back(e);
                vert[b].push_back( new chan(uc+a, e) );
            }
        }
    }

    for (int i = 1; i <= inf; i++) cin >> uc[i].IQ;
    for (int i = 1; i <= mat; i++) cin >> vert[i].IQ;

    while( bfs(&sr,&pr,persons,inf,mat) ) {
        sr.other = sr.begin();
        pr.other = pr.begin();
        for( os *p = persons; p != persons+inf+mat; p++ ) p->other = p->begin();
        dfs(&sr, &pr, numeric_limits<long long>::max());
    }

    int n = 0, m = mat;
    long long ans = 0;

    bfs(&sr,&pr,persons,inf,mat);
    for( int i = 1; i <= inf; i++ ) {
        if( uc[i].pathDist != -1 ) {
            n++;
            ans += uc[i].IQ;
            uc[i].replay = true;
        }
    }

    for (int i = 1; i <= mat; i++) vert[i].replay = true;

    for (int i = 1; i <= inf; i++) {
        if (uc[i].replay) {
            for (int j = 1; j <= mat; j++) {
                if( (vert[j].replay) && (! es[i][j]) ) {
                    m--;
                    vert[j].replay = false;
                }
            }
        }
    }

    for (int i = 1; i <= mat; i++) {
        if(vert[i].replay) {
            ans += vert[i].IQ;
        }
    }

    cout << ans << '\n' << n <<'\n';
    for (int i = 1; i <= inf; i++) {
        if( uc[i].replay ) {
            cout << i <<' ';
        }
    }

    cout <<'\n' << m <<'\n';
    for (int i = 1; i <= mat; i++) {
        if( vert[i].replay ) {
            cout << i << ' ';
        }
    }
    cout <<'\n';
}
