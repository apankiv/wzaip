#include <cmath>
#include <cstdio>
#include <vector>

using namespace std ;

struct point {
    double x;
    double y;
};

double len(point a, point b) {
    return hypot(a.x - b.x, a.y - b.y);
}

inline double getDouble(FILE *f = stdin) {
    char ch;
    bool seen = false;
    bool sign = false;
    char values[10];
    double result =0.;
    bool beforeDot = true;
    int beforeLength = 0;
    double multiplier;

    while((ch = getc(stdin)) != EOF) {
        if(ch == '-') {
            sign = true;
            continue;
        }

        if(ch == ' ' || ch == '\n') {
            if(seen) break;
            continue;
        }

        if(ch == '.') {
            beforeDot = false;
            multiplier = 1.;

            while(beforeLength) {
                result += (double)(values[--beforeLength] - '0') * multiplier;
                multiplier *= 10.;
            }

            multiplier = 10.;
        } else {
            if(!beforeDot) {
                result += double(ch - '0') / multiplier;
                multiplier *= 10.;
            } else {
                values[beforeLength++] = ch;
            }
            seen = true;
        }
    }
    if(beforeDot) {
        multiplier = 1;
        while(beforeLength) {
            result += (double)(values[--beforeLength] - '0') * multiplier;
            multiplier *= 10.;
        }
    }

    if(sign) result *= -1.;

    return result;
}

void test() {
    int n, k;
    scanf("%d %d", &n, &k);
    vector<point> wielokat;
    double pole = 0;

    for (int i = 0; i < n; i++) {
        point t;
        t.x = getDouble();
        t.y = getDouble();
        wielokat.push_back(t);
    }

    int j = n - 1;

    for(int i = 0; i < n; i++) {
        pole += (wielokat[j].x + wielokat[i].x) * (wielokat[j].y - wielokat[i].y);
        j = i;
    }

    pole = fabs(pole);
    pole = pole * 0.5;
    pole += M_PI * k * k;

    for(int i = 0; i < n-1; i++) {
        pole += k * len(wielokat[i], wielokat[i+1]);
    }

    pole += k * len(wielokat[n-1], wielokat[0]);
    printf("%lf\n", pole);
}

int main() {
    int T;
    scanf("%d\n", &T);

    while(T --> 0) {
        test();
    }
}
