#include <cstdio>
#include <vector>

using namespace std;
int n, m;
int max (int a, int b) { return a > b ? a : b; }

void update(vector<int>& t, int i, int value) {
    for (t[i += n] = value; i > 1; i >>= 1)
        t[i >> 1] = max(t[i], t[i ^ 1]);
}

int query (const vector<int>& t, int l, int r) {
    int res = 0;
    r+=1;
    for (l += n, r += n; l < r; l >>= 1, r >>= 1) {
        if (l & 1) res = max(res, t[l++]);
        if (r & 1) res = max(res, t[--r]);
    }
    return res;
}

int main() {
    int t, l, e;
    scanf("%d %d", &n, &m);
    vector<int> tt = vector<int>(2*n+1);
    while (m-->0) {
        scanf("%d %d %d", &t, &l, &e);
        if(t == 1) {
            update(tt, l, e);
        } else {
            printf("%d\n", query (tt, l, e));
        }
    }
}
