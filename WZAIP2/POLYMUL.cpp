#include <complex>
#include <iostream>
#include <vector>
#include <utility>

using namespace std;

const long double PI = 3.141592653589793238462643383279502884L;

void bitReversal(const int n, int *tab) {
    tab[0] = 0;
    tab[1] = n / 2;

    for (int k = 2; k < n; k += 2) {
        tab[k] = (tab[k / 2] >> 1);
        tab[k + 1] = tab[k] ^ tab[1];
    }
}

vector<complex<double> > FFT(vector<complex<double> > &v, const bool inverse) {
    if (v.size() == 1) {
        return vector<complex<double> >(1, v[0]);
    }

    int n = 2;
    while (n < static_cast<int>(v.size())) {
        n *= 2;
    }
    v.resize(n, complex<double>(0));

    complex<double> *W = new complex<double>[n / 2];

    const int mul = (inverse ? -1 : 1);
    const double base = 2 * PI / n;
    for (int i = 1; i < n / 2; ++i) {
        const double arg = base * i;
        W[i] = complex<double>(cos(arg), mul * sin(arg));
    }

    int *order = new int[n];
    bitReversal(n, order);

    vector<complex<double> > result(n);
    for (int i = 0; i < n; ++i) {
        result[i] = v[order[i]];
    }

    delete[] order;

    int k = 2;

    while (k <= n) {
        const int r = k / 2;
        const int m = n / (2 * k);

        for (int i = 0; i < n; i += k) {
            for (int j = i; j < i + r; ++j) {
                const complex<double> tmp = result[j];
                result[j] += result[j + r];
                result[j + r] = tmp - result[j + r];
            }
        }

        if (k != n) {
            for (int i = k; i < n; i += 2 * k) {
                for (int j = 1; j < k; ++j) {
                    result[i + j] *= W[j * m];
                }
            }
        }

        k *= 2;
    }

    if (inverse) {
        for (int i = 0; i < n; ++i) {
            result[i] /= n;
        }
    }

    delete[] W;

    return result;
}

vector<double> multiplyPolynomials(vector<double> &p1, vector<double> &p2) {
    const unsigned int n1 = static_cast<unsigned int>(p1.size());
    const unsigned int n2 = static_cast<unsigned int>(p2.size());
    unsigned int n = n1 + n2 - 1;

    p1.resize(n, 0);
    p2.resize(n, 0);

    vector<complex<double> > k1(n);
    vector<complex<double> > k2(n);

    for (int i = 0; i < static_cast<int>(n); ++i) {
        k1[i] = complex<double>(p1[i]);
        k2[i] = complex<double>(p2[i]);
    }

    vector<complex<double> > f1 = FFT(k1, false);
    const vector<complex<double> > f2 = FFT(k2, false);

    const unsigned int N = static_cast<unsigned int>(f1.size());

    for (int i = 0; i < static_cast<int>(N); ++i) {
        f1[i] *= f2[i];
    }

    vector<complex<double> > inverse = FFT(f1, true);
    const vector<complex<double> > i2 = FFT(inverse, false);

    vector<double> result(n);
    for (int i = 0; i < static_cast<int>(n); ++i) {
        result[i] = inverse[i].real();
    }

    return move(result);
}

int main() {
    ios_base::sync_with_stdio(0);

    int t;
    cin >> t;

    while (t-->0) {
        int n;
        cin >> n;

        vector<double> p1(n + 1);
        vector<double> p2(n + 1);

        for (int i = n; i >= 0; --i) {
            cin >> p1[i];
        }
        for (int i = n; i >= 0; --i) {
            cin >> p2[i];
        }

        vector<double> result = multiplyPolynomials(p1, p2);

        for (int i = 2 * n; i >= 0; --i) {
            cout << static_cast<long long int>(result[i] + 0.5) << " ";
        }
        cout << "\n";
    }

    return 0;
}
