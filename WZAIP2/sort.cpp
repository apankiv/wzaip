#include <cstdio>

using namespace std;

long long arr[1000000];

int partition(long long t[], int l, int r) {
    int pivot   = t[(r+l) / 2],
            i   = l,
            j   = r;

    while (i <= j) {
        while (t[i] < pivot) {
            i++;
        }

        while (t[j] > pivot) {
            j--;
        }

        if (i <= j) {
            long long tmp = t[i];
            t[i] = t[j];
            t[j] = tmp;
            i++;
            j--;
        }
    }

    return i;
}

void sort(long long t[], int l, int r) {
    int index;

    index = partition(t, l, r);
    if (l < index - 1) sort(t, l, index - 1);
    if (index < r) sort(t, index, r);
}

void test () {
    int l;
    scanf("%d", &l);
    for(int i = 0; i < l; i++) scanf("%lld", &arr[i]);
    sort(arr, 0, l-1);
    for(int i = 0; i < l; i++) printf("%lld ", arr[i]);
    printf("\n");
}

int main () {
    int t;
    scanf("%d", &t);
    while(t --> 0) test();
}
