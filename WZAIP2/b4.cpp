#include <cstdio>

void test() {
    int n, t;

    scanf("%d", &n);
    int m[n][n], m2[n][n], m4[n][n];

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            scanf("%d", &t);
            m[i][j] = t;
        }
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            m2[i][j] = 0;
            for(int k = 0; k < n; k++) {
                m2[i][j] += m[i][k] * m[k][j];
            }
        }
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            m4[i][j] = 0;
            for(int k = 0; k < n; k++) {
                m4[i][j] += m2[i][k] * m2[k][j];
            }
        }
    }

    int bad = 0;
    for (int i = 0; i < n; i++) bad += m2[i][i] * (m2[i][i] - 1);

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) bad += m[i][j] * m2[j][j] * m[j][i];
    }

    int ans = 0;
    for(int i = 0; i < n; i++) ans += m4[i][i];

    printf("%d\n", (ans - bad) / 8);

}

int main () {
    int T;
    scanf("%d", &T);

    while (T --> 0) {
        test();
    }

    return 0;
}
