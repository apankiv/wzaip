#include <stdio.h>
#include <string.h>

char kl[111], na[111], diff[111], total[111];

void solve() {
	int len1 = strlen(total);
	int len2 = strlen(diff);
	int a, b, c, i, j, k, f;
	char tmp[111];

	for (i= len1 - 1, j = len2 - 1, k = c=0; i >= 0 || j >= 0 || c; i--, j--, k++) {
		a = i < 0 ? 0 : total[i] - '0';
		b = j < 0 ? 0 : diff[j] - '0';
		tmp[k] = (a + b + c) % 10 + '0';
		c = (a + b + c) / 10;
	}
	tmp[k] = 0;

	strcpy(kl,"0");
	for (i = k - 1, j = a = f = 0; i >= 0; i--) {
		b = (a * 10 + tmp[i] - '0') / 2;
		a = (a * 10 + tmp[i] - '0') % 2;
		if (b) {
            f = 1;
        }
		if (f) {
            kl[j] = b + '0';
            j++;
        }
	}

	if(!j) j++;
	kl[j] = 0;

	for(i = len1 - 1, j = len2 - 1, k = c = 0; i >= 0; i--, j--, k++) {
		a = total[i] - '0';
		b = j >= 0 ? diff[j] - '0' : 0;
		if (a < b + c) {
			tmp[k] = (10 + a - b - c) + '0';
			c = 1;
		} else {
			tmp[k] = a - b - c + '0';
			c = 0;
		}
	}

	tmp[k] = 0;

    strcpy(na,"0");
    for(i = k - 1, j = a = f = 0; i >= 0; i--) {
        b = (a * 10 + tmp[i] - '0') / 2;
        a = (a * 10 + tmp[i] - '0') % 2;
        if (b) {
            f = 1;
        }
        if (f) {
            na[j] = b + '0';
            j++;
        }
    }

    if(!j){
        j++;
    }

    na[j] = 0;
}

int main()
{
	while(scanf("%s %s", total, diff) == 2) {
		solve();
		printf("%s\n%s\n", kl, na);
	}
	return 0;
}
