#include <cstdio>
#include <stdint.h>
#include <cstdlib>
#include <string>

using namespace std;

uint64_t mulmod(uint64_t a, uint64_t b, uint64_t m) {
    uint64_t r = 0;

    for(; a; a >>= 1) {
        if(a & 1) r = (r + b) % m;
        b = (b + b) % m;
    }

    return r;
}

uint64_t modpow(uint64_t a, uint64_t k, uint64_t const &n) {
    uint64_t r = 1;

    for(; k; k >>= 1) {
        if(k & 1) r = mulmod(r, a, n);
        a = mulmod(a, a, n);
    }

    return r;
}

bool MRCT(uint64_t const &n, uint64_t const &d, uint64_t const &r, uint64_t const &a) {
    uint64_t x = modpow(a, d, n);

    if (x == 1) return true;
    if (x == n - 1) return true;

    for (int j = 1; j < r; j++){
        x = mulmod(x, x, n);
        if(x == 1) return false;
        if(x == n - 1) return true;
    }

    return false;
}

bool check(uint64_t num) {
    if (num == 2) return true;
    if (num % 2 == 0) return false;

    if (num < 63) {
        for (int i = 3; i < num; i += 2) {
            if (num % i == 0) return false;
        }
    } else {
        if (num > 2147483646) {
            uint64_t sprp[8] = {2, 325, 9375, 28178, 450775, 9780504, 1795265022, 0};
            uint64_t d = num - 1;
            uint64_t r = 0;

            while (d % 2 == 0){
                d /= 2;
                r++;
            }

             for(int i=0; i<7; i++)
                if (!MRCT(num, d, r, sprp[i]))
                    return false;

            return true;
        } else {
            uint64_t sprp[4] = {2, 7, 61, 0};
            uint64_t d = num - 1;
            uint64_t r = 0;

            while (d % 2 == 0) {
                d /= 2;
                r++;
            }

            for(int i = 0; i < 3; i++)
                if(!MRCT(num, d, r, sprp[i]))
                    return false;
        }
    }

    return true;
}

void test() {
    uint64_t num;
    scanf("%lld", &num);
    bool res = check(num);
    string ans = res ? "YES" : "NO";
    printf("%s\n", ans.c_str());
}

int main () {
    int t;

    scanf("%d", &t);

    while(t --> 0) {
        test();
    }

    return 0;
}
