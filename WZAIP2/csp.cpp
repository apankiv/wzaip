#include <cstdio>
#include <vector>
#include <set>
#include <queue>
#include <cctype>

using namespace std;

struct V {
    set<int> in;
    vector<int> out;
};

typedef vector<V> G;

int readINT() {
  int x = 0;
  int ch;
  while(isspace(ch = getchar_unlocked()));
  for(; isdigit(ch); ch = getchar_unlocked()) x = 10 * x + ch - '0';
  return x;
}

void putUI(int const &n) {

   if(n>0) { //wyłuskanie kolejnych cyfr z liczby n
         putUI(n/10);
         putc_unlocked(n%10+48,stdout);
     }
}

int main(void) {
    int n, m, _, a, b;
    n = readINT();
    m = readINT();
    G v(n);
    _ = m;

wh:
    a = readINT()-1;
    b = readINT()-1;
    v[a].out.push_back(b);
    v[b].in.insert(a);
    if (--_ > 0) goto wh;

    priority_queue<int> __;
    vector<int> solution;

    for (int i = 0; i < n; i++) if (v[i].in.empty()) __.push(-i);

    while (!__.empty()) {
        int ___ = -__.top();
        __.pop();
        solution.push_back(___ + 1);

        for (vector<int>::iterator it = v[___].out.begin(); it != v[___].out.end(); ++it) {
            v[*it].in.erase(___);
            if (v[*it].in.empty()) __.push(-(*it));
        }
    }

    if (solution.size() < n) {
        printf("Sandro fails.\n");
    } else {
        for (vector<int>::iterator it = solution.begin(); it != solution.end(); ++it) {
            putUI(*it);
            putc_unlocked(32, stdout);
        }
    }

    return 0;
}
