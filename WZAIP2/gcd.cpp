#include <cstdio>

using namespace std;

unsigned long long gcd(unsigned long long a, unsigned long long b) {
    return b == 0 ? a : gcd(b, a % b);
}

int main () {
    unsigned long long a, k, m;
    while (true) {
        scanf("%lld %lld", &a, &k);
        if (a == 0 && k == 0) {
            break;
        } else {
            printf("%lld\n", a/gcd(a, k));
        }
    }
}
