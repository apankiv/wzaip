#include <cstdio>

using namespace std;

long long fast_pow(long long a,long long k, long long m){
    long long result = 1;
    long long power = k;
    long long value = a;
    long long mod = m;
    while(power>0)
    {
        if(power&1)
            {result = result*value;
            result = result%mod;}
        value = value*value;
        value = value%mod;
        power /= 2;
    }
    return result;
}

int main () {
    long long a, k, m;
    while (true) {
        scanf("%lld %lld %lld", &a, &k, &m);
        if (a == 0 && k == 0 && m == 0) {
            break;
        } else {
            printf("%lld\n", fast_pow(a, k, m));
        }
    }
}
