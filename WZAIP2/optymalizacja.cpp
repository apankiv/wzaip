#include <iostream>
#include <vector>
#include <fstream>
#include <limits>
#include <random>
#include <set>

#define NUM_RANDOMS 900

using namespace std;

random_device randomDevice;
mt19937 generator(randomDevice());

struct vertex {
	vector<unsigned int> neighbours;
	bool inCover = false;

	void addNeighbour(unsigned int neighbour) {
		neighbours.push_back(neighbour);
	}

	unsigned int randNeighbour() {
		uniform_int_distribution<> distribution(0, neighbours.size() - 1);
		return neighbours[distribution(generator)];
	}
};

struct graph
{
	vector<vertex> vertices;

	void removeAllNeighbours(unsigned int index) {
		for(const unsigned int& i : vertices[index].neighbours) {
			vertices[i].neighbours.erase(find(vertices[i].neighbours.begin(), vertices[i].neighbours.end(), index));
		}
		vertices[index].neighbours.clear();
	}

	unsigned int howMuchNeighbours(unsigned int indexA, unsigned int indexB) {
		set<unsigned int> distinct;
		for(unsigned int& i : vertices[indexA].neighbours) distinct.insert(i);
		for(unsigned int& i : vertices[indexB].neighbours) distinct.insert(i);
		return distinct.size();
	}
};

graph orginalGraph;
graph currGraph;

bool allNeighboursInCover(unsigned int index)
{
	for(const unsigned int& i : orginalGraph.vertices[index].neighbours) {
		if(!currGraph.vertices[i].inCover) return false;
	}
	return true;
}

int main()
{
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);

	ifstream inputFile("test.in");
	unsigned int vertexAmount, edgeAmount;
	inputFile >> vertexAmount >> edgeAmount;
	orginalGraph.vertices.resize(vertexAmount);
	for(unsigned int i = 0; i < edgeAmount; ++i) {
		unsigned int a, b;
		inputFile >> a >> b;
		orginalGraph.vertices[a].addNeighbour(b);
		orginalGraph.vertices[b].addNeighbour(a);
	}
	inputFile.close();

	unsigned int minCover = numeric_limits<unsigned int>::max();

	while(true) {
		currGraph = orginalGraph;
		vector<unsigned int> cover;
		vector<unsigned int> nonRand;
		for(unsigned int i = 0; i < vertexAmount; ++i) nonRand.push_back(i);

		while(!nonRand.empty()) {
			uniform_int_distribution<> distribution(0, nonRand.size()-1);
			unsigned int bestRand;
			unsigned int bestRandNeighbour;
			unsigned int bestRandAmount = 0;
			unsigned int currAmount;
			unsigned int justRand;
			bool doAddToCover = false;
			for(unsigned int j = 0; j < NUM_RANDOMS; ++j) {
				justRand = distribution(generator);
				unsigned int i = nonRand[justRand];
				unsigned int neighbourI;
				if(!currGraph.vertices[i].neighbours.empty()) {
					neighbourI = currGraph.vertices[i].randNeighbour();
					currAmount = currGraph.howMuchNeighbours(i, neighbourI);
					if(currAmount > bestRandAmount) {
						bestRandAmount = currAmount;
						bestRandNeighbour = neighbourI;
						bestRand = justRand;
						doAddToCover = true;
					}
				}
			}
			if(doAddToCover) {
				unsigned int i = nonRand[bestRand];
				cover.push_back(bestRandNeighbour);
				cover.push_back(i);
				currGraph.vertices[bestRandNeighbour].inCover = true;
				currGraph.vertices[i].inCover = true;
				currGraph.removeAllNeighbours(bestRandNeighbour);
				currGraph.removeAllNeighbours(i);
				nonRand.erase(nonRand.begin() + bestRand);
			}
			else nonRand.erase(nonRand.begin() + justRand);
		}
		for(unsigned int i = 0; i < cover.size(); ++i) {
			if(allNeighboursInCover(cover[i])) {
				currGraph.vertices[cover[i]].inCover = false;
				cover.erase(cover.begin() + i);
				--i;
			}
		}

		if(cover.size() < minCover) {
			ofstream out("res.out");
			out << cover.size() << '\n';
			cout << "BEST ";
			for(unsigned int & i : cover) {
				out << i << ' ';
			}
			out.close();
			minCover = cover.size();
		}

		cout << cover.size() << '\n';
	}

	return 0;
}
