#include <cstdio>

using namespace std;

int arr[20001];

void test () {
    int tmp;
    long long l;
    for (int i = 0; i < 20001; i++) arr[i] = 0;
    scanf("%lld", &l);

    for(long long i = 0; i < l; i++){
        scanf("%d", &tmp);
        arr[tmp+10000]++;
    }

    for(int i = 0; i < 20001; i++) {
        if (arr[i] > 0) {
            while (arr[i] --> 0) printf("%d ", i-10000);
        }
    }
    printf("\n");
}

int main () {
    int t;
    scanf("%d", &t);
    while(t --> 0) test();
}
