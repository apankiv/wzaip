#include <cstdio>

using namespace std;

unsigned long long f[10001];
int max = 1;

unsigned long long fact(int p) {
    if (p <= max) return f[p];
    for (int i = max+1; i <= p; i++) {
        f[i] = f[i-1] * i;
        max++;
    }
    return f[p];
}

int main () {
    f[0] = 1;
    f[1] = 1;
    for (int i = 2; i < 10001; i++) f[i] = 0;
    int t, l, tmp;
    scanf("%d", &t);
    while(t --> 0) {
        scanf("%d", &l);
        while(l-->0) {
            scanf("%d", &tmp);
            printf("%lld ", fact(tmp));
        }
        printf("\n");
    }
    return 0;

}
