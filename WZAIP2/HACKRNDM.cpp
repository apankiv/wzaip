#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

int readINT() {
    int x = 0;
    int ch;
    while(isspace(ch = getchar_unlocked()));
    for(; isdigit(ch); ch = getchar_unlocked()) x = 10 * x + ch - '0';
    return x;
}

int main() {
    int n, k, t;
    n = readINT();
    k = readINT();
    vector<int> v;
    for (int i = 0; i < n; i++) {
        t = readINT();
        v.push_back(t);
    }
    int counter = 0;
    sort(v.begin(), v.begin()+n);
    for (auto i = v.begin(); i != v.end(); ++i) {
        for(auto j = i + 1; j != v.end(); ++j) {
            if((*j) - (*i) == k) counter++;
            if((*j) - (*i) > k) break;
        }
    }
    printf("%d\n", counter);
    return 0;
}
