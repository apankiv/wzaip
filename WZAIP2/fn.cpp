#include <bits/stdc++.h>

using namespace std;


int modFix(int const &a, int const &p) {
  int r = a % p;
  if(r < 0) r += p;
  return r;
}


int modMul(int const &a, int const &b, int const &p) {
  return (a * int64_t(b)) % p;
}


int modPow(int const &a, int const &k, int const &p) {
  int r = 1;
  int m = a;
  for(int t = k; t != 0; t /= 2) {
    if(t % 2 == 1) r = modMul(r, m, p);
    m = modMul(m, m, p);
  }
  return r;
}


int legendreSymbol(int const &a, int const &p) {
  return modPow(a, (p - 1) / 2, p);
}


int tonelliShanks(int const &a, int const &p) {
  if(legendreSymbol(a, p) != 1) return -1;

  int q = p - 1, s = 1;
  while(q % 2 == 0) {
    q /= 2;
    ++s;
  }

  if(s == 1) return modPow(a, (p + 1) / 4, p);

  int z = 2;
  while(z < p and legendreSymbol(z, p) != p - 1) ++z;

  int c = modPow(z, q, p);
  int r = modPow(a, (q + 1) / 2, p);
  int t = modPow(a, q, p);
  int m = s;

  while(true) {
    if(t == 1) return modFix(r, p);

    int i = 0;
    int u = t;
    while(u != 1) {
      u = modPow(u, u, p);
      ++i;
    }

    int b = c;
    for(int j = 0; j < m - i - 1; ++j) {
      b = modMul(b, b, p);
    }
    r = modMul(r, b, p);
    c = modMul(b, b, p);
    t = modMul(t, c, p);
    m = i;
  }
}


int extendedEuclid(int const &p, int const &q, int &gp, int &gq) {
  int g = p; gp = 1; gq = 0;
  int h = q, hp = 0, hq = 1;

  while(h != 0) {
    int d = g / h;
    int t = h,             tp = hp,           tq = hq;
        h = g -  h  * d;   hp = gp - hp * d;  hq = gq - hq * d;
        g = t;             gp = tp;           gq = tq;
  }

  return g;
}


int modInv(int const &a, int const &p) {
  int ga, gp;
  extendedEuclid(a, p, ga, gp);
  return modFix(ga, p);
}


int babyStepGiantStep(int const &a, int const &b, int const &p) {
  int m = 0;
  int v = 1;
  map<int, int> T;

  for(; m * m < p; ++m) {
    if(T.find(v) == T.end()) T[v] = m;
    v = modMul(v, a, p);
  }

  int y = b;
  int q = modInv(v, p);
  for(int i = 0; i < m; ++i) {
    if(T.find(y) != T.end()) return modFix(i * m + T[y], p);
    y = modMul(y, q, p);
  }

  return -1;
}


int modRank(int const &a, int const &p) {
  if(a == 1) return 1;

  int r = p - 1;
  int q = p - 1;

  for(int i = 2; i * i <= q; ++i) {
    bool c = true;
    while(q % i == 0) {
      if(c) {
        if(modPow(a, r / i, p) == 1) r /= i;
        else                         c = false;
      }
      q /= i;
    }
  }

  if(modPow(a, r / q, p) == 1) r /= q;

  return r;
}


void test() {
  int c, p;
  cin >> c >> p;

  if(c == 0) {
    cout << "0\n";
    return;
  }

  if(c == 1) {
    cout << "1\n";
    return;
  }

  int h = (p + 1) / 2; // 1 / 2
  int x = tonelliShanks(5, p); // sqrt(5)
  int ix = modInv(x, p); // 1 / x
  int y = modMul(1 + x, h, p); // (1 + sqrt(5)) / 2
  int r = modRank(y, p);
  int z = modMul(c, x, p); // c * x

  int ans = p + 1;

  // even
  {
    int t = tonelliShanks(modFix(modMul(z, z, p) + 4, p), p);

    int yn0 = modMul(modFix(z + t, p), h, p);
    int yn1 = modMul(modFix(z - t, p), h, p);

    int n0 = babyStepGiantStep(y, yn0, p);
    if(n0 != -1 and modFix((yn0 - modInv(yn0, p)) * ix, p) == c) {
      int s = n0 % r;
      if(s % 2 == 1) s += r;
      if(s % 2 == 0) ans = min(ans, s);
    }

    int n1 = babyStepGiantStep(y, yn1, p);
    if(n1 != -1 and modFix((yn1 - modInv(yn1, p)) * ix, p) == c) {
      int s = n1 % r;
      if(s % 2 == 1) s += r;
      if(s % 2 == 0) ans = min(ans, s);
    }
  }

  // odd
  {
    int t = tonelliShanks(modFix(modMul(z, z, p) - 4, p), p);

    int yn0 = modMul(modFix(z + t, p), h, p);
    int yn1 = modMul(modFix(z - t, p), h, p);

    int n0 = babyStepGiantStep(y, yn0, p);
    if(n0 != -1 and modFix((yn0 - modInv(yn0, p)) * ix, p) == c) {
      int s = n0 % r;
      if(s % 2 == 0) s += r;
      if(s % 2 == 1) ans = min(ans, s);
    }

    int n1 = babyStepGiantStep(y, yn1, p);
    if(n1 != -1 and modFix((yn1 - modInv(yn1, p)) * ix, p) == c) {
      int s = n1 % r;
      if(s % 2 == 0) s += r;
      if(s % 2 == 1) ans = min(ans, s);
    }
  }

  if(ans == p + 1) ans = -1;

  cout << ans << '\n';
}


int main() {
  ios_base::sync_with_stdio(false);
#ifndef FORCE_TIE
  cin.tie(NULL);
#endif

  int t;
  cin >> t;
  while(t --> 0) test();

  return 0;
}
