#include <cctype>
#include <cstdio>

using namespace std;

int readINT() {
    int x = 0;
    int ch;
    while(isspace(ch = getchar_unlocked()));
    for(; isdigit(ch); ch = getchar_unlocked()) x = 10 * x + ch - '0';
    return x;
}

void putUI(int const &n) {
    if(n>0) {
        putUI(n/10);
        putc_unlocked(n%10+48,stdout);
    }
}

int main(void) {
    unsigned long int n, f = 9;
    bool t = true;

l:
    f = 9;
    t = true;
    if (scanf("%lu", &n) == -1) goto e;
wh:
    n = (n + f - 1) / f;
    t = !t;
    f = t ? 9 : 2;
    if (n > 1) goto wh;

    if (!t) {
        printf("A\n");
    } else {
        printf("B\n");
    }
    goto l;
e:
    return 0;
}
