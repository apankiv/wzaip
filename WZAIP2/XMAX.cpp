#include <iostream>
#include <iomanip>
#include <math.h>
#include <vector>

using namespace std;

double d = 0;
double det(int n, vector< vector<double> > mat)
{
    int c, subi, i, j, subj;
    vector< vector<double> > submat;

	for(int i = 0; i < n; ++i)
	{
		vector<double> t;
		for(int j =0; j < n; j++) {
			t.push_back(0);
		}
		submat.push_back(t);
	}

    if (n == 2)
    {
        return( (mat[0][0] * mat[1][1]) - (mat[1][0] * mat[0][1]));
    }
    else
    {
        for(c = 0; c < n; c++)
        {
            subi = 0;
            for(i = 1; i < n; i++)
            {
                subj = 0;
                for(j = 1; j < n; j++)
                {
                    if (j == c)
                    {
                        continue;
                    }
                    submat[subi][subj] = mat[i][j];
                    subj++;
                }
                subi++;
            }
        d += (pow(-1 ,c) * mat[0][c] * det(n - 1 ,submat));
        }
    }
    return d;
}

int main()
{
   int i,j,k,n;
   double x,t;
   vector <vector<double> > a;
   cin>>n;
   for(i=0;i<n;i++)
   {
   vector<double> tt;
		for (j = 0; j < n; ++j)
		{
			cin >> x;
			tt.push_back(x);
		}
		a.push_back(tt);
      }


   x = det(n, a);
   if(x == 0)
   cout << "OSOBLIWA\n";
   else{
   for(i=0;i<n;i++)
   {
      for(j=0;j<n;j++)
      {
          if(i==j)
             a[i][j]=1;
         else
             a[i][j]=0;
       }
   }
   for(i=0;i<n;i++)
   {
      t=a[i][i];
      for(j=0;j<n;j++)
          a[i][j]=a[i][j]/t;
      for(j=0;j<n;j++)
      {
         if(i!=j)
         {
            t=a[j][i];
            for(k=0;k<n;k++)
                a[j][k]=a[j][k]-t*a[i][k];
          }
      }
   }
   for(i=0;i<n;i++)
   {
      for(j=0;j<n;j++)
         cout<< fixed << setprecision(3) <<a[i][j] << " ";
      cout<<"\n";
    }
   }
return 0;
}
