#include <vector>

using namespace std;

template <typename T> class BinaryHeap {
    private:
        vector<T> _data;
    public:
        BinaryHeap();
        void insert(T newValue);
        int size();
};
