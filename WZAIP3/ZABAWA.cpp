#include <stdint.h>
#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

typedef unsigned int uint;


uint32_t fast_pow_mod(uint32_t a, uint32_t k, uint32_t const &n) {
    uint32_t ret = 1;
    for(; k; k >>= 1) {
        if(k & 1) ret = (ret * uint64_t(a)) % n;
        a = (a * uint64_t(a)) % n;
    }
    return ret;
}


bool is_prime(uint const &n) {
    static uint const W[] = {2, 3, 0};
    uint64_t d = n - 1;
    uint s = 0;
    if(n < 2) return false;
    while((d & 1) == 0) { d >>= 1; ++s; }
    for(uint const *w = W; *w != 0; ++w) {
        if(n == *w) return true;
        bool composite = true;
        uint32_t wd = fast_pow_mod(*w, d, n);
        if(wd == 1) {
            composite = false;
        } else {
            for(uint r = 0; r < s; ++r) {
                if(wd == n - 1) {
                    composite = false;
                    break;
                }
                wd = (wd * uint64_t(wd)) % n;
            }
        }
        if(composite) {
            return false;
        }
    }
    return true;
}

bool lower(string const &a, string const &b) {
    if(a.length() < b.length()) return true;
    if(a.length() > b.length()) return false;
    return a < b;
}


string closest_palindrome(string const &y) {
    string t(y);
    unsigned int l = y.length();
    unsigned int h = (l - 1) / 2;
    for(unsigned int i = 0; i <= h; ++i) t[l - 1 - i] = t[i];
    return t;
}


string next(string const &p) {
    unsigned int l = p.length();
    if(count(p.begin(), p.end(), '9') == l) {
        string r(l + 1, '0');
        r[l] = r[0] = '1';
        return r;
    }
    unsigned int h = (l - 1) / 2;
    string t(p);
    while(t[h] == '9') { t[l - 1 - h] = t[h] = '0'; --h; }
    t[l - 1 - h] = (t[h] = (t[h] + 1));
    return t;
}


string prev(string const &p) {
    unsigned int l = p.length();
    if(count(p.begin(), p.end(), '0') == l - 2 and p[0] == '1' and p[l - 1] == '1') {
        string r(l - 1, '9');
        return r;
    }
    unsigned int h = (l - 1) / 2;
    string t(p);
    while(t[h] == '0') { t[l - 1 - h] = t[h] = '9'; --h; }
    t[l - 1 - h] = (t[h] = (t[h] - 1));
    return t;
}


void print_prev_palindrome(string const &y) {
    string p = closest_palindrome(y);
    if(lower(p, y)) cout << p << '\n';
    else cout << prev(p) << '\n';
}


void print_next_palindrome(string const &y) {
    string p = closest_palindrome(y);
    cerr << y << ' ' << p << endl;
    if(lower(y, p)) cout << p << '\n';
    else cout << next(p) << '\n';
}


int main() {
    ios_base::sync_with_stdio(false);

    int x;
    string y;

    while(cin >> x >> y) {
        if(x < 2) continue;
        if(is_prime(x)) print_next_palindrome(y);
        else print_prev_palindrome(y);
    }

    return 0;
}
