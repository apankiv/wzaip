#include <iostream>
#include <vector>

using namespace std;

int pos = 0;

int process (int start, int end, int l, int u) {
    int t, d = end - start;
    for (int i = 0; i <= d; ++i) {
        t = pos + i + (i - d);
        if (t > l && t < u) {
            pos = t;
            return i;
        }
    }
    return -1;
}

int main () {
    ios_base::sync_with_stdio(false);
    int start, finish, a, b, c, t;
    cin >> start >> finish;
    start = 0;
    int res = 0;
    bool can = true;
    while (cin >> a >> b >> c) {
gohere:
        t = process(start, a, b, c);
        if (t != -1) { 
            res += t;
        } else {
            res++;
            pos += 2;
            if (pos < c) goto gohere;
            else can = false;
        }
        start = a;
    }
    if (can == true) cout << res;
    else cout << "NIE";
    return 0;
}

