#include <iostream>
#include <vector>
#include <stdint.h>

using namespace std;

vector<uint64_t> factorials;

inline void initFact() {
    factorials.push_back(1);
    for (int i = 1; i < 16; ++i) {
        factorials.push_back(factorials[i-1] * i);
    }
}

int main () {
    initFact();
    uint64_t a, i, t, _;
    cin >> a;
    i = 0;
loop:
    i++;
    if (i < factorials.size() && factorials[i] < a) goto loop;

    if (factorials[i] == a) {
        cout << (i != 0 ? i : 1) << endl;
    } else {

    }
    return 0;
}
