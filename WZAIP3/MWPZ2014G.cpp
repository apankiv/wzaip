#include <iostream>
#include <vector>

using namespace std;
#define ull unsigned long long

ull fast_pow(ull a,ull k, ull m){
    ull result = 1;
    ull power = k;
    ull value = a;
    ull mod = m;
    while(power>0)
    {
        if(power&1)
            {result = result*value;
            result = result%mod;}
        value = value*value;
        value = value%mod;
        power /= 2;
    }
    return result;
}

int main () {
    ull t;
    cin >> t;
    while (t --> 0) {
        ull tmp, m, k;
        cin >> m >> k;
        vector<ull> mi;
        for (int i = 0; i < m; ++i) {
            cin >> tmp;
            mi.push_back(tmp);
        }
        for (int i = mi.size() - 2; i >= 0; --i) {
            mi[i] = fast_pow(mi[i], mi[i+1], 1000000);
        }
        cout << fast_pow(k, mi[0], 1000000) << '\n';
    }
}
