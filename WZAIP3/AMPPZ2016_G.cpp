#include <map>
#include <iostream>
#include <string>

using namespace std;

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int d, q;
    char action;
    string num;
    int l, t;
    map<string, int> car_pos;
    map<int, int> pos_len;
    cin >> d >> q;
    bool placed = false;
    while (q --> 0) {
        cin >> action;

        if (action == 'O') {
            cin >> num;
            auto it = car_pos.find(num);
            if (it != car_pos.end()) {
                pos_len.erase(it->second);
                car_pos.erase(num);
                cout << "OK\n";
            } else {
                cout << "BRAK\n";
            }
        } else {
            cin >> num >> l;
            if (pos_len.empty()) {
                if (l <= d) {
                    car_pos.insert(pair<string, int>(num, 0));
                    pos_len.insert(pair<int,int>(0, l));
                    cout << "0\n";
                } else {
                    cout << "NIE\n";
                }
            } else {
                placed = false;
                int t = 0;
                for (auto it = pos_len.begin(); it != pos_len.end() && !placed; ++it) {
                    //cout << "t + l = " << t + l << endl;
                    //cout << "it = " << it->first << endl;
                    if (it->first > t && it->first >= t + l) {
                        car_pos.insert(pair<string, int>(num, t));
                        pos_len.insert(pair<int,int>(t, l));
                        cout << t << endl;
                        placed = true;
                    } else {
                        t = it->first + it->second;
                    }
                }
                if (!placed) {
                    if (t + l < d) {
                        car_pos.insert(pair<string, int>(num, t));
                        pos_len.insert(pair<int,int>(t, l));
                        cout << t << endl;
                    } else {
                        cout << "NIE\n";
                    }
                }
            }

        }
    }
    return 0;
}
