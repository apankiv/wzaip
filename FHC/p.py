import math as m

t = int(input())
for i in range(t):
    n = int(input())
    p = []
    for j in range(n):
        p.append(int(input()))
    p.sort()
    counter = 0
    while len(p) > 0:
        #print(p)
        #print(counter)
        tmp = p.pop()
        if tmp >= 50:
            counter+=1
            continue
        if tmp == 25 and len(p) > 0:
            counter +=1
            del p[:1]
            continue
        count = 50//tmp
        if count == len(p):
            counter+=1
            p = []
        elif count > len(p):
            p = []
        else:
            counter+=1
            del p[:count]

    print("Case #%d: %d" % (i+1, counter)) 

