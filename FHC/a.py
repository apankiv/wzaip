import math as m

def solve(p, x, y):
    r = m.sqrt(x**2 + y**2)
    if r > 50:
        return False
    rads = m.atan2(x,y)
    radc = m.atan2(y,x)
    procs = rads*100/(2*m.pi)
    procc = radc*100/(2*m.pi)
    #print(p, x, y, rads, radc, procs, procc)
    if procs > 0 and procc > 0:
        return (25 - procc <= p)
    if procs > 0 and procc < 0:
        return (25 - procc <= p)
    if procs < 0 and procc < 0:
        return (25-procc <= p)
    if procs < 0 and procc > 0:
        return (100 + procs <= p)


    

t = int(input())
for i in range(t):
    p, x, y = map(int, input().split(' '))
    res = solve(p,x-50,y-50)
    print("Case #%d: %s" % (i+1, "black" if res == True else "white")) 
